'use strict';

module.exports = {
  randomInt, shuffle };

function randomInt(low,high) {
   return Math.floor(Math.random() * (high - low) + low);

}

function shuffle(names){
  let currentIndex = names.length;
  while(currentIndex !== 0) {
    let randomIndex = randomInt(0,names.length);
    currentIndex--;

    let tempvalue = names[currentIndex];
    names[currentIndex] = names[randomIndex];
    names[randomIndex] = tempvalue;
  }
  return names;
}
