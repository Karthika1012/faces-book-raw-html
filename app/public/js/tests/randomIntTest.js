'use strict';

module.exports = testRandomInt;

const randomInt = require('./../randomInt.js');
const assert = require('assert');


function testRandomInt() {
  test_randomInt_with_lowerbound_included_upperbound_excluded();

}

function test_randomInt_with_lowerbound_included_upperbound_excluded() {
  let count = [0, 0, 0, 0, 0, 0, 0];
  for(let i = 0 ; i < 1000; i++) {
    let a = randomInt.randomInt(1,7);
    count[a]++;
  }
  assert.ok(count[1] > 0);
  assert.ok(count[2] > 0);
  assert.ok(count[3] > 0);
  assert.ok(count[4] > 0);
  assert.ok(count[5] > 0);
  assert.ok(count[6] > 0);
}

function shuffle_names_tests() {
  let names = ["Karthika","Thamizh","Supriya","Rishi", "Keerthana"];
  let original = names.slice(0);
      assert.notEqual(randomInt.shuffle(names), original);
}

testRandomInt();
shuffle_names_tests();
console.log("All tests passed");
