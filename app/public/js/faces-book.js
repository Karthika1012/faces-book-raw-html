$(()=> {

const faces = $('faces');
const makeFace = function(name) {
  return $("<img>", {
    title:name,
    src:`img/2018/${name}.jpg`,
    class:"face"
  });
};
 const names = [
   "Akshat",
   "Karthika",
   "Sanjana",
   "Mahidher",
   "Akhil",
   "Keerthana",
   "RVishnuPriya",
   "Anushree",
   "Mariah",
   "Aishu",
   "Jon",
   "Pavithrann",
   "Rishi",
   "Sindhu",
   "Supriya",
   "Shruti",
   "Santhosh",
   "Gayatri",
   "Vishnu",
   "Varsha",
   "John",
   "Ameya",
   "Sudeep",
   "Janani",
   "Thiru",
   "Thamizh"];
 names.forEach(function(name) {
   faces.append(makeFace(name));
});

});
